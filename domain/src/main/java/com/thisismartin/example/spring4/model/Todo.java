package com.thisismartin.example.spring4.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Entity
@Table(name = "T_TODO")
public class Todo extends SuperEntity {

	public Todo() {
	}

	@NotNull
	@Size(min = 3, max = 300)
	@Column(name = "title", length = 300)
	private String title;

	@Column(name = "completed")
	private boolean completed;

	@Column(name = "email")
	private String email;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Todo [getTitle()=").append(getTitle())
				.append(", isCompleted()=").append(isCompleted())
				.append(", getEmail()=").append(getEmail())
				.append(", getUuid()=").append(getUuid()).append("]");
		return builder.toString();
	}

}

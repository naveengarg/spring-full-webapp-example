package com.thisismartin.example.spring4.controller.web;

import java.security.Principal;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thisismartin.example.spring4.model.Todo;
import com.thisismartin.example.spring4.service.TodoService;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Controller
public class MainController {

	private static final Log logger = LogFactory.getLog(MainController.class);

	@Resource
	private TodoService todoService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getHomePage(Model model) {

		model.addAttribute("todo", new Todo());

		logger.info("Returning main page;");
		return "main";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String createTodo(@Valid @ModelAttribute Todo todo,
			BindingResult bindingResult, Principal principal) {

		if (bindingResult.hasErrors()) {
			return "main";
		}

		logger.info("Creating todo: " + todo);
		todoService.create(todo, principal);

		logger.info("Redirecting to list;");
		return "redirect:/list";
	}

}

package com.thisismartin.example.spring4.config;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.thisismartin.example.spring4.security.CustomUserAuthenticationProvider;

/**
 * @author Martin Spasovski (@moondowner)
 */
@EnableWebSecurity
public class SecurityConfig {

	@Inject
	private CustomUserAuthenticationProvider authProvider;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(authProvider);
	}

	@Configuration
	public static class FormLoginWebSecurityConfigurerAdapter extends
			WebSecurityConfigurerAdapter {

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring().antMatchers("/resources/**");
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.formLogin().defaultSuccessUrl("/list")
					.loginPage("/login")
					.loginProcessingUrl("/j_spring_security_check")
					.failureUrl("/login-error")
					.permitAll()
					.and()
					//
					.logout()
					.logoutUrl("/logout")
					.deleteCookies("JSESSIONID")
					.and()
					//
					.authorizeRequests().antMatchers("/resources/**")
					.permitAll().antMatchers("/register").permitAll()
					.antMatchers("/error").permitAll().anyRequest()
					.authenticated();
		}
	}

	@Configuration
	@Order(1)
	public static class WebApiSecurityConfigurationAdapter extends
			WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.antMatcher("/api/**").authorizeRequests().anyRequest()
					.authenticated().and().httpBasic();
		}
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}

package com.thisismartin.example.spring4.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Controller
public class LoginController {

	@RequestMapping("login")
	public String getLoginPage() {
		return "login";
	}

	@RequestMapping("login-error")
	public String getLoginErrorPage(Model model) {
		model.addAttribute("hasError", true);
		return "login";
	}

}

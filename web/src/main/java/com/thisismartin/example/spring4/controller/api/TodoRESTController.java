package com.thisismartin.example.spring4.controller.api;

import java.security.Principal;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.ImmutableList;
import com.thisismartin.example.spring4.model.Todo;
import com.thisismartin.example.spring4.service.TodoService;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Controller
@RequestMapping(value = "/api")
public class TodoRESTController {

	@Resource
	private TodoService todoService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Todo> findAll(Principal principal) {
		return ImmutableList.copyOf(todoService.searchByUser(principal));
	}
	
	

}

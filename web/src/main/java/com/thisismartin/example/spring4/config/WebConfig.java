package com.thisismartin.example.spring4.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.thisismartin.example.spring4" })
@Import({ ThymeleafConfig.class, PersistenceConfig.class, ExternalConfig.class,
		SecurityConfig.class })
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/resources/img/**").addResourceLocations(
				"/resources/img/");
		registry.addResourceHandler("/resources/css/**").addResourceLocations(
				"/resources/css/");
		registry.addResourceHandler("/resources/js/**").addResourceLocations(
				"/resources/js/");
	}

	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {

		configurer.enable();
	}

}
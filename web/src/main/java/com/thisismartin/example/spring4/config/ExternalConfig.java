package com.thisismartin.example.spring4.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Configuration
public class ExternalConfig {

	@Profile(Constants.SPRING_PROFILE_DEVELOPMENT)
	@Configuration
	@PropertySource("classpath:application.properties")
	public static class LocalDBProperties {
	}

	@Profile(Constants.SPRING_PROFILE_PRODUCTION)
	@Configuration
	@PropertySource("classpath:application-prod.properties")
	public static class RemoteDBProperties {
	}

	@Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}

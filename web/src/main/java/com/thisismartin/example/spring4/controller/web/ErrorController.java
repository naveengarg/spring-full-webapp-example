package com.thisismartin.example.spring4.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;
import com.thisismartin.example.spring4.exception.UserAlreadyRegisteredException;

/**
 * @author Martin Spasovski (@moondowner)
 */
@ControllerAdvice
public class ErrorController {

	private static final Log logger = LogFactory.getLog(ErrorController.class);

	@ExceptionHandler(value = { Exception.class, RuntimeException.class })
	public ModelAndView getErrorPage(HttpServletRequest request, Exception e) {
		logger.error("Logging exception", e);
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("message", Throwables.getRootCause(e));
		return modelAndView;
	}

	@ExceptionHandler(value = UserAlreadyRegisteredException.class)
	public ModelAndView getErrorPageUserAlreadyRegistered(
			HttpServletRequest request, Exception e) {
		logger.error("Logging exception", e);
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("message", "User is already registered. \n\n"
				+ Throwables.getRootCause(e));
		return modelAndView;
	}
}

package com.thisismartin.example.spring4.controller.web;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import com.codeborne.selenide.WebDriverRunner;

/**
 * Main flow test, uses Selenide and PhantomJS(Web)Driver.
 * 
 * @author Martin Spasovski (@moondowner)
 */
// TODO
// @RunWith(SpringJUnit4ClassRunner.class)
// @ActiveProfiles(profiles = Constants.SPRING_PROFILE_DEVELOPMENT)
// @ContextConfiguration(classes = { WebConfig.class })
// @WebAppConfiguration
public class MainAcceptanceTest {

	private static final Log logger = LogFactory
			.getLog(MainAcceptanceTest.class);

	@Test
	public void mainFlowTest() throws Exception {
		// open app
		open("http://localhost:8080/app/");

		// check if redirected on login page
		// check by field
		$(By.cssSelector("#username")).should(exist);
		// check by url
		Assert.assertTrue(WebDriverRunner.getWebDriver().getCurrentUrl()
				.contains("login"));

		// generate random string
		String testUsernameAndPass = UUID.randomUUID().toString();
		logger.info("TEST WITH USERNAME: " + testUsernameAndPass);

		// register
		open("http://localhost:8080/app/register");
		$(By.cssSelector("#email")).setValue(testUsernameAndPass);
		$(By.cssSelector("#password")).setValue(testUsernameAndPass);
		// submit registration
		$(By.cssSelector("#password")).submit();

		// check if redirected to login page
		$(By.cssSelector("#username")).should(exist);

		// login
		$(By.cssSelector("#username")).setValue(testUsernameAndPass);
		$(By.cssSelector("#password")).setValue(testUsernameAndPass);
		$(By.cssSelector("#password")).submit();

		// check if redirected to main page
		$(By.cssSelector("#todoDescription")).should(exist);

		// add new todo
		$(By.cssSelector("#todoDescription")).setValue("TEST DESCRIPTION.");
		$(By.cssSelector("#todoDescription")).submit();

		// check if redirected to todo list page
		Assert.assertTrue(WebDriverRunner.getWebDriver().getCurrentUrl()
				.contains("list"));
	}

}

package com.thisismartin.example.spring4.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thisismartin.example.spring4.model.User;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Repository
public interface UserRepository extends CrudRepository<User, UUID> {

	User findByEmail(String userEmail);
}

package com.thisismartin.example.spring4.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thisismartin.example.spring4.model.Todo;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Repository
public interface TodoRepository extends JpaRepository<Todo, UUID> {

	@Query("select t from Todo t where t.email = :email and UPPER(t.title) like %:searchParameter%")
	Iterable<Todo> findByEmailAndTitleLike(@Param("email") String email,
			@Param("searchParameter") String searchParameter);
	
}

package com.thisismartin.example.spring4.exception;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class TodoNotFoundException extends Exception {

	private static final long serialVersionUID = 3969150775472714349L;

}
